<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSharesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shares', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('support_ticket_no');
            $table->string('contact_email');
            $table->string('company');
            $table->string('application');
            $table->string('priority'); //3, high 2 medium 1 Low
            $table->string('support_category');           
            // $table->string('image')->nullable(); //files
            $table->longText('support_description');
            $table->string('created_by');
            $table->string('status')->default('0'); //not resolved.
            //file upload
            $table->string('filename')->nullable();
            $table->string('mime')->nullable();
            $table->string('original_filename')->nullable();
            //end of file upload
            $table->string('solution_status')->nullable(); //not resolved.
            $table->string('resolved_by')->nullable(); //not resolved.
            //manhours
            $table->integer('man_hours')->nullable(); //not resolved.
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shares');
    }
}
