@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">

        @auth
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Item sold -->
                <div class="card stats-card">
                    <div class="stats-icon bg-success">
                        <span class="ti-user"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_users}}</span></div>
                        <span class="desc">Total Users</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Item sold -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Earnings -->
                <div class="card stats-card">
                    <div class="stats-icon bg-danger">
                        <span class="ti-user"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">Coming Soon</span></div>
                        <span class="desc">Total Admins</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Earnings -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Messages -->
                <div class="card stats-card">
                    <div class="stats-icon bg-info">
                        <span class="ti-user"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">coming soon </span></div>
                        <span class="desc">Active Users</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Messages -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Notifications -->
                <div class="card stats-card">
                    <div class="stats-icon bg-secondary">
                        <span class="ti-user"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">Coming Soon</span></div>
                        <span class="desc">InActive Users</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Notifications -->

            </div><!-- .col -->
        </div><!-- .row -->
        <div class="row">
            <div class="col-12">

                <!-- Invoice -->
                <div class="card">
                    <div class="card-body">
                        <div class="card-header-inside">
                            Service Tickets
                        </div>

                        <table id="mytable" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>User Since</th>
                                </tr>
                            </thead>
                            <tbody>



                                @foreach($users as $client)
                                <tr>
                                    <td>{{ $client->name }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ $client->email }}</td>
                                    <td>{{ \Carbon\Carbon::parse($client->created_at)->format('d/M/Y')}}</td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>

                    </div><!-- .card-body -->
                </div><!-- .card -->
                <!-- /End Invoice -->


            </div><!-- .col -->
        </div><!-- .row -->
        @else

        @endauth


        @include('sweetalert::alert')
    </div><!-- .content -->
</main>
@stop