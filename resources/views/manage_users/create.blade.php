@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">


        <div class="row">
            <div class="col-md-12 col-lg-6">

                <!-- Form row -->
                <div class="card">
                    <div class="card-header">Create New User</div>
                    <div class="card-body">

                        <!--
						The .form-group class is the easiest way to add some structure to forms. Its only purpose is to provide margin-bottom around a label and control pairing. As a bonus, since it's a class you can use it with <fieldset>s, <div>s, or nearly any other element.
						You may also swap .row for .form-row, a variation of our standard grid row that overrides the default column gutters for tighter and more compact layouts.
						-->
                        <form method="post" action="{{ route('manage_users.store') }}">
                            @csrf

                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Name</label>
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="inputAddress" class="col-form-label">Email</label>
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input id="password" type="password" hidden value="Tamarix2020" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <!-- <label for="inputAddress2" class="col-form-label">PasswordConfirmation</label> -->
                                <input id="password-confirm" hidden value="Tamarix2020" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>


                            <div class="form-group">
                                <!-- <label for="inputAddress2" class="col-form-label">status</label> -->
                                <!-- <input type="text" value="0" hidden value="Tamarix@2030" readonly class="form-control" hidden id="inputAddress2" name="status" placeholder="Apartment, studio, or floor"> -->
                            </div>
                            <button type="submit" class="btn btn-primary">Create User</button>
                        </form>

                    </div><!-- .card-body -->
                </div><!-- .card -->
                <!-- /End Form row -->

            </div><!-- .col -->


        </div><!-- .row -->



        @include('sweetalert::alert')
    </div><!-- .content -->
</main>
@stop