@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">
        @auth
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card stats-card">
                    <div class="stats-icon bg-success">
                        <span class="ti-bag"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$manhourstoday}}</span></div>
                        <span class="desc">Daily Man Hours</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card stats-card">
                    <div class="stats-icon bg-danger">
                        <span class="ti-wallet"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_notresolved}}</span></div>
                        <span class="desc">Weekly Man Hours</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card stats-card">
                    <div class="stats-icon bg-info">
                        <span class="ti-email"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_new}}</span></div>
                        <span class="desc">Monthly Man Hours</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card stats-card">
                    <div class="stats-icon bg-secondary">
                        <span class="ti-save"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_total}}</span></div>
                        <span class="desc">Average Man Hours</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header-inside">
                            Resolved Service Tickets Man Hours
                        </div>
                        <table id="mytable" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>Ticket Number</th>
                                    <th>Company</th>
                                    <th>Application</th>
                                    <th>Ticket Created by</th>
                                    <th>Opening Date</th>
                                    <th>Closing Date</th>
                                    <th>Contact Email</th>
                                    <th>Man Hours</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($requests as $request)
                                <tr>
                                    <td>{{ $request->support_ticket_no }}</td>
                                    <td>{{ $request->company }}</td>
                                    <td>{{ $request->application }}</td>
                                    <td>{{ $request->created_by}}</td>
                                    <td>{{ \Carbon\Carbon::parse($request->created_at)->format('d/M/Y')}}</td>
                                    <td>{{ \Carbon\Carbon::parse($request->updated_at)->format('d/M/Y')}}</td>
                                    <td>{{ $request->contact_email }}</td>
                                    <td>{{ $request->man_hours }}</td>
                                    @if ($request->status == 0)
                                    <td><button class="btn btn-danger">Not Resolved</button></td>
                                    @elseif($request->status ==1)
                                    <td><button class="btn btn-success">Resolved</button></td>
                                    @endif

                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @else
        <p>Kindly login, you cannot view thispage without login </p>
        @endauth
        @include('sweetalert::alert')
    </div>
</main>
@stop