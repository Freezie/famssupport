<!DOCTYPE html>
<html lang="en-US">
<head>
    <title>Tamarix Support Center</title>
    @include('shared.links')
    @laravelPWA
</head>
<body>
    <div id="page-container">
        @include('shared.navv')
        @include('shared.header')
        @include('shared.aside')
        @yield('content')
        @include('shared.footer')
    </div>
    @include('shared.scripts')
</body>
</html>