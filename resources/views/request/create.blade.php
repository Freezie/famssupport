@extends('layouts.master')

@section('content')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5d146a6536eab97211197632/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>

<!--End of Tawk.to Script-->
<!-- <script src="//code.jivosite.com/widget/aykLjIgIRq" async></script> -->
<main id="main-container">
    <div class="content">
        <h2 class="content-heading">Create a Support Ticket</h2>
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">Create a Support Request</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('request.store') }}" enctype='multipart/form-data'>
                            @csrf
                            <div class="form-group">
                                <label for="inputAddress" class="col-form-label">Support Ticket Number</label>
                                <input type="text" readonly value="<?php echo date("Ymdhms"); ?>" class="form-control" id="support_ticket_no" name="support_ticket_no" placeholder="1234 Main St">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Select Company</label>
                                <select id="inputState" class="form-control" name="company" required>
                                    <option value="">Choose Company</option>
                                    @foreach($project as $p)
                                    <option value="{{$p->company}}">{{$p->company}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Select Application/Project</label>
                                <select id="inputState" class="form-control" name="application" required>
                                    <option value="">Choose Application/Project</option>
                                    @foreach($project as $p)
                                    <option value="{{$p->application}}">{{$p->application}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Service Priority</label>
                                <select id="inputState" class="form-control" name="priority" required>
                                    <option value="">Choose Priority</option>
                                    <option value="1">High (System Completely Unavailable | Response: < 2 Hours)</option> <option value="2">Medium (Critical System Functionality Unavailable | Response: 2-6 Hours)</option>
                                    <option value="3">Low (General System Issue | Response: < 24 Hours)</option> </select> </div> <div class="form-group">
                                            <label for="inputAddress2" class="col-form-label">Ticket Created by</label>
                                            <input type="text" required class="form-control" id="inputAddress2" name="created_by" placeholder="Enter Your Name">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress" class="col-form-label">Contact Email</label>
                                <input type="email" required class="form-control" id="inputAddress" name="contact_email" placeholder="Enter Your Email Address">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Support Request Title</label>
                                <input type="text" required class="form-control" id="inputAddress2" name="support_category" placeholder="Support Request Title/ Title of Issue">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Attach File <i>(Max upload file size 4MB)</i></label>
                                <input type="file" class="form-control" id="inputAddress2" name="bookcover" placeholder="Attach File (Files max 5Mb)">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Support Request Description (Max charactrs: 500)</label>
                                <textarea class="form-control" name="support_description" rows=11 cols=50 maxlength=500 required></textarea>
                            </div>

                            <div class="form-group">
                                <input type="text" value="0" readonly class="form-control" hidden id="inputAddress2" name="status" placeholder="Apartment, studio, or floor">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit Request</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @include('sweetalert::alert')
    </div>
</main>

@endsection