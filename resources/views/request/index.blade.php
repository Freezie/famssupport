@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">
        @auth
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card stats-card">
                    <div class="stats-icon bg-success">
                        <span class="ti-bag"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_resolved}}</span></div>
                        <span class="desc">Resolved Tickets</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card stats-card">
                    <div class="stats-icon bg-danger">
                        <span class="ti-wallet"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_notresolved}}</span></div>
                        <span class="desc">Tickets Not Resolved</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card stats-card">
                    <div class="stats-icon bg-info">
                        <span class="ti-email"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_new}}</span></div>
                        <span class="desc">New Tickets Today</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="card stats-card">
                    <div class="stats-icon bg-secondary">
                        <span class="ti-save"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_total}}</span></div>
                        <span class="desc">Total Tickets</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header-inside">
                            Service Tickets
                        </div>
                        <table id="mytable" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>Ticket Number</th>
                                    <th>Company</th>
                                    <th>Application</th>
                                    <th>Ticket Created by</th>
                                    <th>Creation Date</th>
                                    <th>Contact Email</th>
                                    <th>Support Title</th>
                                    <th>Support Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($requests as $request)
                                <tr>
                                    <td>{{ $request->support_ticket_no }}</td>
                                    <td>{{ $request->company }}</td>
                                    <td>{{ $request->application }}</td>
                                    <td>{{ $request->created_by}}</td>
                                    <td>{{ \Carbon\Carbon::parse($request->created_at)->format('d/M/Y')}}</td>
                                    <td>{{ $request->contact_email }}</td>
                                    <td>{{ $request->support_category }}</td>
                                    <td>{{ $request->support_description }}</td>
                                    @if ($request->status == 0)
                                    <td><button class="btn btn-danger">Not Resolved</button></td>
                                    @elseif($request->status ==1)
                                    <td><button class="btn btn-success">Resolved</button></td>
                                    @endif
                                    <td><a href="{{ route('request.edit',$request->id)}}" class="btn btn-primary">View Ticket</a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @else
        <div class="row">
            <div class="col-md-12 col-lg-6">
                <div class="card">
                    <div class="card-header">Create a Support Request</div>
                    <div class="card-body">
                        <form method="post" action="{{ route('request.store') }}" enctype='multipart/form-data'>
                            @csrf
                            <div class="form-group">
                                <label for="inputAddress" class="col-form-label">Support Ticket Number</label>
                                <input type="text" readonly value="<?php echo date("Ymdhms"); ?>" class="form-control" id="support_ticket_no" name="support_ticket_no" placeholder="1234 Main St">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Select Company</label>
                                <select id="inputState" class="form-control" name="company" required>
                                    <option value="">Choose Company</option>
                                    @foreach($project as $p)
                                    <option value="{{$p->company}}">{{$p->company}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Select Application/Project</label>
                                <select id="inputState" class="form-control" name="application" required>
                                    <option value="">Choose Application/Project</option>
                                    @foreach($project as $p)
                                    <option value="{{$p->application}}">{{$p->application}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Service Priority</label>
                                <select id="inputState" class="form-control" name="priority" required>
                                    <option value="">Choose Priority</option>
                                    <option value="1">High (System Completely Unavailable | Response: < 2 Hours)</option> <option value="2">Medium (Critical System Functionality Unavailable | Response: 2-6 Hours)</option>
                                    <option value="3">Low (General System Issue | Response: < 24 Hours)</option> 
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Ticket Created by</label>
                                <input type="text" required class="form-control" id="inputAddress2" name="created_by" placeholder="Enter Your Name">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress" class="col-form-label">Contact Email</label>
                                <input type="email" required class="form-control" id="inputAddress" name="contact_email" placeholder="Enter Your Email Address">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Support Request Title</label>
                                <input type="text" required class="form-control" id="inputAddress2" name="support_category" placeholder="Support Request Title/ Title of Issue">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Attach File <i>(Max upload file size 4MB)</i></label>
                                <input type="file" class="form-control" id="inputAddress2" name="bookcover" placeholder="Attach File (Files max 5Mb)">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Support Request Description</label>
                                <textarea class="form-control" name="support_description" rows=11 cols=50 maxlength=250 required></textarea>
                            </div>
                            <div class="form-group">
                                <input type="text" value="0" readonly class="form-control" hidden id="inputAddress2" name="status" placeholder="Apartment, studio, or floor">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit Request</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        @endauth
        @include('sweetalert::alert')
    </div>
</main>
@stop