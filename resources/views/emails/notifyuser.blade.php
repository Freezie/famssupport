<!DOCTYPE html>
<html>

<head>
    <title>Support Notification</title>
</head>

<body>
    <p>Hello,</p>
    <p>Thank you for contacting us.</p>
    <p>Our technical support team is looking into your request.</p>
    <p>Typically this will take less than 24 hours or on the next business day.</p>
    <p>Please refer to the <b>Support Ticket Number</b> on the <b>subject</b> when speaking with any Tamarix Support Representative.</p>
    <p>If you do not see this e-mail in your Inbox, please check your Spam folder and, if necessary, add support@tamarix.co.ke to your e-mail program’s white list.</p>
    <p>Thanks again for contacting us. </p>
    <p>We’ll be in touch very soon.</p>
    <br>
    <p>Regards,</p>
    <p>Tamarix Support Team,</p>
    <p>support@tamarix.co.ke.</p>
</body>

</html>