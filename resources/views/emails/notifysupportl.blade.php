</html>

<!DOCTYPE html>
<html>

<head>
    <title>Support Notification</title>
</head>

<body>
    <p>Hi there,</p>
    <p>A support ticket has been created.</p>
    <p>Kindly look into it.</p>
    <p> The ticket is on <b style="color:forestgreen;">Low Priority</b>. A Workaround is to be provided within <b>Twenty-four (24) hours</b>.</p>
    <p>Thank you.</p>
    <br>
    <p>Regards,</p>
    <p>Tamarix Support Team,</p>
    <p>support@tamarix.co.ke.</p>
</body>

</html>