<!DOCTYPE html>
<html>

<head>
    <title>Support Notification</title>
</head>

<body>
    <p>Hello,</p>
    <p>Thank you for contacting us.</p>
    <p>Our technical support team is looking into your request.</p>
    <p> Your ticket is on <b style="color:red;">High Priority.</b> A full solution will be provided within <b>Two (2) hours</b> of receipt and acknowledgement of this service request.</p>
    <p>Please refer to the <b>Support Ticket Number</b> on the <b>Email Subject</b> when speaking with any Tamarix Support Representative.</p>
    <!--<p><i style="color:DodgerBlue;">If you do not see this e-mail in your Inbox, please check your Spam folder and, if necessary, add support@tamarix.co.ke to your e-mail program’s white list.</i></p> -->
    <p>Thanks again for contacting us.</p>
    <br>
    <p>Regards,</p>
    <p>Tamarix Support Team,</p>
    <p>support@tamarix.co.ke.</p>
</body>

</html>