<!DOCTYPE html>
<html>

<head>
    <title>Support Notification</title>
</head>

<body>
    <p>Hi there,</p>
    <p>A support ticket has been created.</p>
    <p>Kindly look into it.</p>
    <p> The ticket is on <b style="color:red;">High Priority.</b> A full solution will be provided within <b>Two (2) hours</b> of receipt and acknowledgement</b>.</p>
    <p>Thank you.</p>
    <br>
    <p>Regards,</p>
    <p>Tamarix Support Team,</p>
    <p>support@tamarix.co.ke.</p>
</body>

</html>