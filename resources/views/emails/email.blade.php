<!DOCTYPE html>
<html>

<head>
    <title>Tamarix Support</title>
</head>

<body>

    <p>Hello,</p>
    <!-- <p>This is where the problem description will go</p>
    <p><b><u>Solution:</b><u></p>
    <p>The resolution goes here</p>
    <br>
    <p><i>Support Team<i></p> -->

    <p>

        We've resolved your support request.</p>
    <p>
        Is there anything else we can help you with? Please don't hesitate to reply to this email if you have any other questions.</p>
    <p>

        We will proceed and mark this ticket as <b>"Resolved"</b>.</p>
    <p>

        Please feel welcome to reopen this ticket or open a new one if you need any further assistance.</p>

    <p>Thanks again for working with us!</p>

    <p>Regards,</p>
    <p>

    Tamarix Support Team,</p>
    
    <p>support@tamarix.co.ke</p>


</body>

</html>