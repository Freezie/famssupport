@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">


        <div class="row">
            <div class="col-md-12 col-lg-6">

                <!-- Form row -->
                <div class="card">
                    <div class="card-header">Create company and Application Record</div>
                    <div class="card-body">

                        <!--
						The .form-group class is the easiest way to add some structure to forms. Its only purpose is to provide margin-bottom around a label and control pairing. As a bonus, since it's a class you can use it with <fieldset>s, <div>s, or nearly any other element.
						You may also swap .row for .form-row, a variation of our standard grid row that overrides the default column gutters for tighter and more compact layouts.
						-->
                        <form method="post" action="{{ route('client.store') }}">
                            @csrf

                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Company</label>
                                <input type="text" class="form-control" id="company" name="company" placeholder="Company">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress" class="col-form-label">Project</label>
                                <input type="text" class="form-control" id="application" name="application" placeholder="Project">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Project Start Date</label>
                                <input type="date" class="form-control" id="date_started" name="date_started" placeholder="Date Sarted">
                            </div>
                            <div class="form-group">
                                <label for="inputAddress2" class="col-form-label">Project End Date</label>
                                <input type="date" class="form-control" id="date_ended" name="date_ended" placeholder="Date Ended">
                            </div>


                            <div class="form-group">
                                <!-- <label for="inputAddress2" class="col-form-label">status</label> -->
                                <input type="text" value="0" readonly class="form-control" hidden id="inputAddress2" name="status" placeholder="Apartment, studio, or floor">
                            </div>
                            <button type="submit" class="btn btn-primary">Submit Request</button>
                        </form>

                    </div><!-- .card-body -->
                </div><!-- .card -->
                <!-- /End Form row -->

            </div><!-- .col -->


        </div><!-- .row -->



        @include('sweetalert::alert')
    </div><!-- .content -->
</main>
@stop