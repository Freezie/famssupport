@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">

        @auth
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Item sold -->
                <div class="card stats-card">
                    <div class="stats-icon bg-success">
                        <span class="ti-bag"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_companies}}</span></div>
                        <span class="desc">Companies</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Item sold -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Earnings -->
                <div class="card stats-card">
                    <div class="stats-icon bg-danger">
                        <span class="ti-wallet"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_project}}</span></div>
                        <span class="desc">Total Applications</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Earnings -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Messages -->
                <div class="card stats-card">
                    <div class="stats-icon bg-info">
                        <span class="ti-email"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_project_complete}}</span></div>
                        <span class="desc">Complete Applications</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Messages -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Notifications -->
                <div class="card stats-card">
                    <div class="stats-icon bg-secondary">
                        <span class="ti-save"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_project_incomplete}}</span></div>
                        <span class="desc">Incomplete Applications</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Notifications -->

            </div><!-- .col -->
        </div><!-- .row -->
        <div class="row">
            <div class="col-12">

                <!-- Invoice -->
                <div class="card">
                    <div class="card-body">
                        <div class="card-header-inside">
                            Service Tickets
                        </div>

                        <table id="mytable" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Application</th>
                                    <th>Date Started</th>
                                    <th>Date Ended</th>
                                </tr>
                            </thead>
                            <tbody>



                                @foreach($clients as $client)
                                <tr>
                                    <td>{{ $client->company }}</td>
                                    <td>{{ $client->application }}</td>
                                    <td>{{ \Carbon\Carbon::parse($client->date_started)->format('d/M/Y')}}</td>
                                    <td>{{ \Carbon\Carbon::parse($client->date_ended)->format('d/M/Y')}}</td>
                                </tr>
                                @endforeach


                            </tbody>
                        </table>

                    </div><!-- .card-body -->
                </div><!-- .card -->
                <!-- /End Invoice -->


            </div><!-- .col -->
        </div><!-- .row -->
        @else

        @endauth


        @include('sweetalert::alert')
    </div><!-- .content -->
</main>
@stop