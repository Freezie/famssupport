@extends('layouts.master')
@section('content')
<main id="main-container">
    <div class="content">

        @auth
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Item sold -->
                <div class="card stats-card">
                    <div class="stats-icon bg-success">
                        <span class="ti-bag"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_resolved}}</span></div>
                        <span class="desc">Resolved Tickets</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Item sold -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Earnings -->
                <div class="card stats-card">
                    <div class="stats-icon bg-danger">
                        <span class="ti-wallet"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_notresolved}}</span></div>
                        <span class="desc">Tickets Not Resolved</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Earnings -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Messages -->
                <div class="card stats-card">
                    <div class="stats-icon bg-info">
                        <span class="ti-email"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_new}}</span></div>
                        <span class="desc">New Tickets Today</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Messages -->

            </div><!-- .col -->
            <div class="col-lg-3 col-md-6 col-sm-6">

                <!-- Notifications -->
                <div class="card stats-card">
                    <div class="stats-icon bg-secondary">
                        <span class="ti-save"></span>
                    </div>
                    <div class="stats-ctn">
                        <div class="stats-counter"><span class="counter">{{$count_total}}</span></div>
                        <span class="desc">Total Tickets</span>
                    </div>
                </div><!-- .card -->
                <!-- /End Notifications -->

            </div><!-- .col -->
        </div><!-- .row -->
        <div class="row">
            <div class="col-12">

                <!-- Invoice -->
                <div class="card">
                    <div class="card-body">
                        <div class="card-header-inside">
                            Service Tickets
                        </div>

                        <table id="mytable" class="table table-striped table-responsive">
                            <thead>
                                <tr>
                                    <th>Ticket Number</th>
                                    <th>Company</th>
                                    <th>Application</th>
                                    <th>Ticket Created by</th>
                                    <th>Creation Date</th>
                                    <th>Contact Email</th>
                                    <th>Support Title</th>
                                    <th>Support Description</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                    <!-- <th>Resolved By</th>
                                    <th>Resolution Date</th>
                                    <th>Resolution Description</th>
                                    <th>Status</th> -->
                                </tr>
                            </thead>
                            <tbody>



                                @foreach($requests as $request)
                                <tr>

                                    <td>{{ $request->support_ticket_no }}</td>
                                    <td>{{ $request->company }}</td>
                                    <td>{{ $request->application }}</td>
                                    <td>{{ $request->created_by}}</td>
                                    <td>{{ \Carbon\Carbon::parse($request->created_at)->format('d/M/Y')}}</td>
                                    <td>{{ $request->contact_email }}</td>
                                    <td>{{ $request->support_category }}</td>
                                    <td>{{ $request->support_description }}</td>
                                    @if ($request->status == 0)
                                    <td><button class="btn btn-danger">Not Resolved</button></td>
                                    @elseif($request->status ==1)
                                    <td><button class="btn btn-success">Resolved</button></td>
                                    @endif
                                    <!-- <td><button class="btn btn-primary">Notify Resolved</button></td> -->
                                    <td><a href="{{ route('shares.edit',$request->id)}}" class="btn btn-primary">View Ticket</a></td>


                                    <!-- <td>
                                        <form class="patch" action="{{ route('shares.edit', $request->id)}}" method="post">
                                            @csrf
                                            @method('PATCH')
                                            <button class="btn delete btn-danger" type="submit">Notify Resolved</button>
                                        </form>
                                        <script>
                                            $(".delete").on("submit", function() {
                                                return confirm("Do you want to delete this item?");
                                            });
                                        </script>
                                    </td> -->






                                </tr>
                                @endforeach


                            </tbody>
                        </table>

                    </div><!-- .card-body -->
                </div><!-- .card -->
                <!-- /End Invoice -->


            </div><!-- .col -->
        </div><!-- .row -->
        @else
        <div class="row">
            <div class="col-md-12 col-lg-6">

                <!-- Form row -->
                <div class="card">
                    <div class="card-header">Create a Support Request</div>
                    <div class="card-body">

                        <!--
						The .form-group class is the easiest way to add some structure to forms. Its only purpose is to provide margin-bottom around a label and control pairing. As a bonus, since it's a class you can use it with <fieldset>s, <div>s, or nearly any other element.
						You may also swap .row for .form-row, a variation of our standard grid row that overrides the default column gutters for tighter and more compact layouts.
						-->
                        <form method="post" action="{{ route('requests.store') }}">
                            @csrf
                            @include('shared.submit_form')
                        </form>

                    </div><!-- .card-body -->
                </div><!-- .card -->
                <!-- /End Form row -->

            </div><!-- .col -->


        </div><!-- .row -->
        @endauth


        @include('sweetalert::alert')
    </div><!-- .content -->
</main>
@stop