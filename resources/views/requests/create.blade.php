@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">

                    <form method="post" action="{{ route('requests.store') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('ticket_number') }}</label>

                            <div class="col-md-6">
                                <input id="ticket_number" type="text" class="form-control" name="support_ticket_no" required autofocus>


                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('contact_number') }}</label>

                            <div class="col-md-6">
                                <input id="ticket_number" type="text" class="form-control" name="contact_email" required autofocus>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('support_category') }}</label>

                            <div class="col-md-6">
                                <input id="ticket_number" type="text" class="form-control" name="support_category" required autofocus>


                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('support_description') }}</label>

                            <div class="col-md-6">
                                <input id="ticket_number" type="text" class="form-control" name="support_description" required autofocus>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('support_description') }}</label>

                            <div class="col-md-6">
                                <input id="ticket_number" type="text" class="form-control" name="created_by" required autofocus>

                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('support_description') }}</label>

                            <div class="col-md-6">
                                <input id="ticket_number" type="text" value="0" class="form-control" name="status" required autofocus>

                            </div>
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Add new request') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
