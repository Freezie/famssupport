<footer id="page-footer" class="pagefooter">
    <div class="content">
        <div class="row">
            <div class="copyright col-sm-12 col-md-12 col-lg-6">
                &copy; Tamarix Support Request developed by <a href="https://www.tamarix.co.ke/" target="_blank">Tamarix Company Limited</a></div>
            <div class="footer-nav col-sm-12 col-md-12 col-lg-6">
                <a href="javascript:void(0)">About</a>
                <a href="javascript:void(0)">Terms</a></div>
        </div>
    </div>
</footer>