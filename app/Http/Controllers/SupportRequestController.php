<?php

namespace App\Http\Controllers;

use App\SupportRequest;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class SupportRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getGuzzleRequest() //get request
    {
        $client = new \GuzzleHttp\Client();
        // $request = $client->get('http://myexample.com');
        $request = $client->get('http://10.10.4.214:9001/ords/apex_ebs_extension/fams/support/');
        $response = $request->getBody();
        $response = json_decode($response);

        // dd($response);
        // return view('requests.index');
        return view('requests.index', compact('response'));
    }
    
    
    public function deleteGuzzleRequest()  //delete request
    {
        $client = new \GuzzleHttp\Client();
        $url = "http://myexample.com/api/posts/1";
        $request = $client->delete($url);
        $response = $request->send();

        dd($response);
    }
    //  public function index()
    // {
    //     //
    //     if (session('success_message')) {
    //         Alert::success('Record Submitted', session('success_message'));
    //     }
    //     return view ('requests.index');
    // }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('requests.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            // 'ticket_number' => 'required',
            // 'contact_number' => 'required',
            // 'support_category' => 'required',
            // 'support_description' => 'required',
            // 'status' => 'required|integer'

            'support_ticket_no' => 'required',
            'contact_email' => 'required',
            'company' => 'required',
            'application' => 'required',
            'priority' => 'required',
            'support_category' => 'required',
            'support_description' => 'required',
            'created_by' => 'required',
            'status' => 'required|integer'
        ]);
        $share = new Share([
            // 'ticket_number' => $request->get('ticket_number'),
            // 'contact_number' => $request->get('contact_number'),
            // 'support_category' => $request->get('support_category'),
            // 'support_description' => $request->get('support_description'),
            // 'status' => $request->get('status')

            'support_ticket_no' => $request->get('support_ticket_no'),
            'contact_email' => $request->get('contact_email'),
            'company' => $request->get('company'),
            'application' => $request->get('application'),
            'priority' => $request->get('priority'),
            'support_category' => $request->get('support_category'),
            'support_description' => $request->get('support_description'),
            'created_by' => $request->get('created_by'),
            'status' => $request->get('status')
        ]);
        $share->save();

        Mail::to($request['contact_email'])->send(new RequestReceived($share));


        return redirect('/')->withSuccessMessage('We have received your support request. Kindly check your email for confirmation');
        return $share;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SupportRequest  $supportRequest
     * @return \Illuminate\Http\Response
     */
    public function show(SupportRequest $supportRequest)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SupportRequest  $supportRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(SupportRequest $supportRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SupportRequest  $supportRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SupportRequest $supportRequest)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SupportRequest  $supportRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(SupportRequest $supportRequest)
    {
        //
    }
}
