<?php

namespace App\Http\Controllers;

use App\Client;
use App\Share;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    public function index()
    {
        //
        if (session('success_message')) {
            Alert::success('Thank you', session('success_message'));
        }
        $requests = Share::orderBy('status')->get();

        $clients = Client::all();
        //These go to Client View
        $project = Client::all();
        $count_project = Client::count();
        $count_companies = Client::count();
        $count_project_complete = Client::whereNotNull('date_ended')->count();
        $count_project_incomplete = Client::whereNull('date_ended')->count();
        //ENd of client view

        // $checkins = Checkin::whereDate('created_at', Carbon::today())->get();
        return view('client.index', compact('clients','count_project_complete', 'count_project_incomplete', 'count_project', 'count_companies'));
        // return view ('client.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view ('client.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'company' => 'required',
            'application' => 'required',
            'date_started' => 'required',
            
        ]);

        $client = new Client([

            'company' => $request->get('company'),
            'application' => $request->get('application'),
            'date_started' => $request->get('date_started'),
            'date_ended' => $request->get('date_ended'),
        ]);
        $client->save();
        return redirect('/client')->withSuccessMessage('You have Successfully submitted the Project Details.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Client  $client
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
        //
    }
}
