<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Share;
use Illuminate\Support\Facades\DB;
use App\Client;
use Illuminate\Support\Carbon;

class ManhourController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    public function index()
    {        
        $requests = Share::where('status', 1)->orderBy('status')->get();
        $group_mail = DB::table('users')->orderByDesc('id');
        $manhourstoday = Share::whereDate('updated_at', Carbon::today())->sum("man_hours");
        $manhoursweek = Share::whereDate('updated_at', Carbon::today())->sum("man_hours");
        $manhoursmonth = Share::whereDate('updated_at', Carbon::today())->sum("man_hours");
        $total_manhours = Share::where('status', 1)->count();
        $count_notresolved = Share::where('status', 0)->count();
        $count_new = Share::whereDate('created_at', Carbon::today())->where('status', 0)->count();
        $new_tickets = Share::whereDate('created_at', Carbon::today())->where('status', 0)->get();
        $count_total = Share::all()->count();
        $count_manhours_today = Share::whereDate('updated_at', Carbon::today())->where('status', 0)->get();
        $project = Client::all();
        return view('man_hours.index', compact('requests', 'group_mail', 'manhourstoday', 'count_notresolved', 'count_new', 'count_total', 'new_tickets', 'project'));
    }
}
