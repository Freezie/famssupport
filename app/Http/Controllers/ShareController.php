<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\RequestReceived;
use App\Mail\SendNotification;
use App\Share;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use App\Mail\TicketRespond;
use Dotenv\Loader;
use App\Client;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class ShareController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    public function index()
    {
        if(session('success_message')){
            Alert::success('Thank you', session('success_message'));
        }
        $requests = Share::orderBy('status')->get();
        $group_mail = DB::table('users')->orderByDesc('id');        
        $count_resolved = Share::where('status', 1)->count();    //1 resolved 0 not resolved
        $count_notresolved = Share::where('status', 0)->count();
        $count_new = Share::whereDate('created_at', Carbon::today())->where('status', 0)->count();
        $new_tickets = Share::whereDate('created_at', Carbon::today())->where('status', 0)->get(); 
        $count_total = Share::all()->count();
        $project = Client::all();
        return view ('shares.index', compact ('requests', 'group_mail', 'count_resolved', 'count_notresolved', 'count_new', 'count_total', 'new_tickets', 'project'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('shares.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'support_ticket_no' => 'required',
            'contact_email' => 'required',
            'company' => 'required',
            'application' => 'required',
            'priority' => 'required',
            'status' => 'required|integer',
            'bookcover' => 'image|max:5120'
        ]);
        $cover = $request->file('bookcover');
        if (!empty($cover)) {
            $extension = $cover->getClientOriginalExtension();
            Storage::disk('public')->put($cover->getFilename() . '.' . $extension,  File::get($cover));
            $share = new Share();
            $share->support_ticket_no = $request->support_ticket_no;
            $share->contact_email = $request->contact_email;
            $share->company = $request->company;
            $share->application = $request->application;
            $share->priority = $request->priority;
            $share->support_category = $request->support_category;
            $share->support_description = $request->support_description;
            $share->created_by = $request->created_by;
            $share->status = $request->status;
            $share->mime = $cover->getClientMimeType();
            $share->original_filename = $cover->getClientOriginalName();
            $share->filename = $cover->getFilename() . '.' . $extension;
        }else{
            $share = new Share();
            $share->support_ticket_no = $request->support_ticket_no;
            $share->contact_email = $request->contact_email;
            $share->company = $request->company;
            $share->application = $request->application;
            $share->priority = $request->priority;
            $share->support_category = $request->support_category;
            $share->support_description = $request->support_description;
            $share->created_by = $request->created_by;
            $share->status = $request->status;            
        }
        $share->save();        
        $contact_email = $share->contact_email = $request->get('contact_email');
        $email = $contact_email;
        $group_mail = ['humphreybrian43@gmail.com', 'muvictormo@gmail.com', 'harmfullbe@gmail.com','humphrey.ochieng@tamarix.co.ke'];
        $ticketno = $share->support_ticket_no = $request->get('support_ticket_no');
        $subject = "Support Request Ticket Number: $ticketno";
        $data['ticket_number'] = $ticketno;  
        $priority =  $share->priority = $request->priority;
        if ($priority == 1){
            Mail::send('emails.high', $data, function ($data) use ($email, $subject) {
                $data->to($email)->subject($subject);
            });
            Mail::send('emails.notifysupporth', $data, function ($data) use ($group_mail, $subject) {
                $data->to($group_mail)->subject($subject);
            });
        } elseif ($priority == 2){
            Mail::send('emails.medium', $data, function ($data) use ($email, $subject) {
                $data->to($email)->subject($subject);
            });
            Mail::send('emails.notifysupportm', $data, function ($data) use ($group_mail, $subject) {
                $data->to($group_mail)->subject($subject);
            }); 
        }else{
            Mail::send('emails.low', $data, function ($data) use ($email, $subject) {
                $data->to($email)->subject($subject);
            });
            Mail::send('emails.notifysupportl', $data, function ($data) use ($group_mail, $subject) {
                $data->to($group_mail)->subject($subject);
            }); 
        }
        return redirect('/')->withSuccessMessage('We have received your support request. Kindly check your email for confirmation');
        return $share;
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Share  $share
     * @return \Illuminate\Http\Response
     */
    public function show(Share $share)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Share  $share
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (session('success_message')) {
            Alert::success('Thank you', session('success_message'));
        }
        $share = Share::find($id);
        $count_new = Share::whereDate('created_at', Carbon::today())->where('status', 0)->count();
        $new_tickets = Share::whereDate('created_at', Carbon::today())->where('status', 0)->get(); 
        return view('shares.edit', compact('share', 'count_new', 'new_tickets'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Share  $share
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'status' => 'required|integer',
            'solution_status' => 'required',            
        ]);
        $share = Share::find($id);
        $share->support_ticket_no = $request->get('support_ticket_no');
        $share->contact_email = $request->get('contact_email');
        $share->support_category = $request->get('support_category');
        $share->support_description = $request->get('support_description');
        $share->status = $request->get('status');
        $share->solution_status = $request->get('solution_status');
        $share->resolved_by = $request->get('resolved_by');   
        $share->man_hours = $request->get('man_hours');
        $share->save();
        $ticketno = $share->support_ticket_no = $request->get('support_ticket_no');
        $problem_description = $share->support_description = $request->get('support_description');
        $support_description = $share->solution_status = $request->get('solution_status');
        $contact_email = $share->contact_email = $request->get('contact_email');
        $data['title'] = "This is Test Mail Tuts Make";
        $data['ticket_number'] = $ticketno;
        $data['problem']= $problem_description;
        $data['support_description']= $support_description;
        $subject = "Solution for Ticket Number: $ticketno";
        $email = $contact_email;
        Mail::send('emails.email', $data, function ($data) use ($email, $subject) {
            $data->to($email)->subject($subject);
        });  
        return redirect('/')->withSuccessMessage('You have Successfully Responded to the Support Request');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Share  $share
     * @return \Illuminate\Http\Response
     */
    public function destroy(Share $share)
    {
        //
    }
}
