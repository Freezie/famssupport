<?php

namespace App\Http\Controllers;

use App\User_Manager;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;
use App\User;



class UserManagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
    }
    public function index()
    {
        //
        if (session('success_message')) {
            Alert::success('Thank you', session('success_message'));
        }
        $users = User::all();
        $count_users = User::count();
        return view ('manage_users.index', compact('users','count_users'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view ('manage_users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => 'required|email|max:255|unique:users|regex:/(.*)tamarix\.co\.ke$/i',
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        $client = new User([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password'))
        ]);
        $client->save();
        return redirect('/manage_users')->withSuccessMessage('You have Successfully Added a new User. Default Password:Tamarix2020');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\User_Manager  $user_Manager
     * @return \Illuminate\Http\Response
     */
    public function show(User_Manager $user_Manager)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User_Manager  $user_Manager
     * @return \Illuminate\Http\Response
     */
    public function edit(User_Manager $user_Manager)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User_Manager  $user_Manager
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User_Manager $user_Manager)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User_Manager  $user_Manager
     * @return \Illuminate\Http\Response
     */
    public function destroy(User_Manager $user_Manager)
    {
        //
    }
}
