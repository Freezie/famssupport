<?php

namespace App;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    //
    protected $guarded = [];
}
