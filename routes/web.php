<?php



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome', function () {
//     return view('welcome');
// });


// Auth::routes();
Auth::routes([
    'verify' => true,
    'register' => false,
    ]);

Route::resource('/', 'RequestController');
Route::resource('request', 'RequestController');

Route::resource('shares', 'ShareController');



// Route::resource('requests', 'SupportRequestController');
// Route::get('requests', 'SupportRequestController@getGuzzleRequest');


Route::resource('welcome', 'ShareController@getGuzzleRequest');

//Add clients
Route::resource('client', 'ClientController');
//manage users
Route::resource('manage_users', 'UserManagerController');
///
Route::resource('books', 'BookController');
//Man Hours
Route::resource('man_hours',  'ManhourController');

//change passsword routes
Route::get('change-password', 'ChangePasswordController@index');
Route::post('change-password', 'ChangePasswordController@store')->name('change.password');


